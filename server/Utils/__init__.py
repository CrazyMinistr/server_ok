from copy import deepcopy
from django.http import HttpResponse
import json

__author__ = 'crazyministr'


class JsonResponse(HttpResponse):
    def __init__(self, data, mimetype='application/json charset=UTF-8',
                 status_code=200):
        super(JsonResponse, self).__init__(content=json.dumps(data),
                                           mimetype=mimetype)
        self.content_decoded = data
        self.status_code = status_code


def get_to_dict(get_request):
    data = deepcopy(dict(get_request))
    for i in data:
        data[i] = data[i][0]
    return data
