from django.contrib import admin
from accounts.models import Account


class AccountAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'artists')


admin.site.register(Account, AccountAdmin)
