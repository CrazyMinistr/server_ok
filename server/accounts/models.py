from django.db import models


class Account(models.Model):
    user_id = models.PositiveIntegerField(max_length=200)
    artists = models.TextField(max_length=1024)
