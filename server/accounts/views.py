from copy import deepcopy
from django.shortcuts import render

# Create your views here.
import json
from Utils import JsonResponse, get_to_dict
from accounts.models import Account
from server.constants import (JSON_NOT_FOUND_ID, JSON_STATUS_OK,
                              RESERVED_ACCOUNT_KEYS)


def accounts(request):
    data = get_to_dict(request.GET)
    if not 'id' in data:
        return JsonResponse(JSON_NOT_FOUND_ID)

    artists = ''
    for key in data:
        if key in RESERVED_ACCOUNT_KEYS:
            continue
        artists += key + ':' + data[key] + ','

    account, created = Account.objects.get_or_create(user_id=int(data['id']))
    account.artists = artists
    account.save()

    return JsonResponse(JSON_STATUS_OK)
