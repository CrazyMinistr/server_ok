JSON_NOT_FOUND_ID = {'status': 'error', 'error': 'not found id'}
JSON_STATUS_OK = {'status': 'ok'}

# friends
COUNT_TOPS = 50
# constants percents by count of matches
# calculate as PERCENT[count_matches] +- rand()
#           0  1  2   3   4   5   6   7   8   9   10  11  12  13
PERCENTS = [7, 9, 12, 15, 20, 30, 40, 50, 60, 70, 80, 85, 90, 95]

# accounts
RESERVED_ACCOUNT_KEYS = ['id']
