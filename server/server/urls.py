from django.conf.urls import patterns, include, url

from django.contrib import admin
from accounts.views import accounts
from friends.views import friends
from main.views import test
from search_friends.views import search_friends
from gen_random_users.views import gen_random_users

admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^test/', test, name="test-view"),
    url(r'^accounts/', accounts, name="accounts"),
    url(r'^friends/', friends, name="friends"),
    url(r'^search_friends', search_friends, name="search-friends"),
    url(r'^gen_random_users', gen_random_users, name="gen-random-users"),
)
