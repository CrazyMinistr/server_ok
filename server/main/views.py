import json
from pprint import pprint
from urllib2 import urlopen
from Utils import JsonResponse


jsessionid = 'jsessionid=a4b1212e8816dd7784eccc8424f54dd32d7e2c655d68a02c.10410cac'

def test(request):
    url_my_music = 'http://wmf.odnoklassniki.ru/my;' + jsessionid
    url_artists = 'http://wmf.odnoklassniki.ru/artist;' + jsessionid + '?artistId=' + '60539216802355'
    url_albums = 'http://wmf.odnoklassniki.ru/album;' + jsessionid + '?albumId=' + '39555612893974'
    url_pop = 'http://wmf.odnoklassniki.ru/pop;' + jsessionid

    # json_url = urlopen(url_my_music)
    # json_url = urlopen(url_artists)
    # json_url = urlopen(url_albums)
    json_url = urlopen(url_pop)

    json_text = json.loads(json_url.read())
    f = open("temp_file.json", "w")
    f.write(json.dumps(json_text, indent=4))
    f.close()
    return JsonResponse(json_text)
