# coding=utf-8
from copy import deepcopy
from django.shortcuts import render
import random
from Utils import JsonResponse
from accounts.models import Account


ARTISTS = ['eminem', 'tropkillaz', 'guf', u'нагора', 'comedoz', 'yellowclaw',
           'avicii', 'noizemc', u'гамора', 'threedaysgrace', 'nickelback',
           'skillet', 'davidguetta', u'каста', '50cent']


def gen_random_users(request):
    for user_id in xrange(1, 101):
        artists = deepcopy(ARTISTS)
        random.shuffle(artists)

        artist = ''
        cnt = random.randint(0, 12)
        for i in xrange(cnt):
            artist += artists[i] + ':13,'

        # print user_id, artist
        account, created = Account.objects.get_or_create(user_id=int(user_id))
        account.artists = artist
        account.save()

    return JsonResponse({'status': 'complete'})
