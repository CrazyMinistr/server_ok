import json
from pprint import pprint
from Utils import JsonResponse
from friends.views import friends
from accounts.views import accounts
from server.constants import JSON_STATUS_OK


def search_friends(request):
    json_status = accounts(request).content_decoded
    if json_status['status'] == 'error':
        return JsonResponse({'status': 'error',
                             'error': json_status['error']})

    friends_list = friends(request).content_decoded
    return JsonResponse(friends_list)
