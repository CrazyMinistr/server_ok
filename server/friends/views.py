from django.shortcuts import render

import json
from pprint import pprint
from random import random
from Utils import JsonResponse, get_to_dict
from accounts.models import Account
from server.constants import (COUNT_TOPS, JSON_NOT_FOUND_ID, PERCENTS)


def get_artists(my_id, user_id):
    my_artists = Account.objects.get(user_id=my_id).artists
    user_artists = Account.objects.get(user_id=user_id).artists

    my_artists = my_artists.split(',')[:-1]
    user_artists = user_artists.split(',')[:-1]

    if not my_artists or not user_artists:
        return [], []

    artists = {}
    for artist in my_artists:
        artists[artist.split(':')[0]] = artist.split(':')[1]
    my_artists = artists

    artists = {}
    for artist in user_artists:
        artists[artist.split(':')[0]] = artist.split(':')[1]
    user_artists = artists
    return my_artists, user_artists


def stupid_algorithm(count_of_matches):
    return int(count_of_matches / 13.0 * 100.0)


def random_algorithm(count_of_matches):
    random_percent = int(random() * 10)
    return max(int(random() * 5) + 7,
               int(PERCENTS[count_of_matches] - random_percent))


def calc_percent(my_id, user_id):
    data = {'id': str(user_id), 'percent': '0'}
    my_artists, user_artists = get_artists(my_id, user_id)
    if not my_artists or not user_artists:
        return data

    count_of_matches = 0
    for artist in my_artists:
        if artist in user_artists:
            count_of_matches += 1

    # data['percent'] = stupid_algorithm(count_of_matches)
    data['percent'] = str(random_algorithm(count_of_matches))
    return data


def my_compare(x, y):
    return int(y['percent']) - int(x['percent'])


def sort_friends(friends_list):
    friends_list = sorted(friends_list, cmp=my_compare)

    if len(friends_list) > COUNT_TOPS:
        friends_list = friends_list[:COUNT_TOPS]

    return friends_list


def friends(request):
    data = get_to_dict(request.GET)
    if not 'id' in data:
        return JsonResponse(JSON_NOT_FOUND_ID)

    friends_list = []
    accounts = Account.objects.all()
    for account in accounts:
        if int(account.user_id) == int(data['id']):
            continue

        result_percents = calc_percent(data['id'], account.user_id)
        if result_percents['percent'] != '0':
            friends_list.append(result_percents)

    friends_list = sort_friends(friends_list)
    return JsonResponse({'friends': friends_list})
